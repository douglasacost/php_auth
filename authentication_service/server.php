<?php
include_once(dirname(__FILE__) . '/../vendor/autoload.php');

use prodigyview\system\Security;
use prodigyview\network\Socket;

//Contains user, password, and the privileges that user has
$logins = array(
	'1' => array('user' => 'user1', 'password' => 'abc123', 'privileges' => array('*')),
	'2' => array('user' => 'user2', 'password' => '123456', 'privileges' => array('*')),
	'3' => array('user' => 'user3', 'password' => 'qwerty', 'privileges' => array('*')),
);

//Mock tokens for the system. Should also be stored in a database
$tokens = array();

include('functions.php');

//For the demo, we are going to store a mock token
storeToken('39e9289a5b8328ecc4286da11076748716c41ec', $tokens, strtotime('+1 hour'));

//Create The Server
$server = new Socket('localhost', 8600, array(
	'bind' => true,
	'listen' => true
));

//Start The Server
$server->startServer('', function ($message) {

	echo "Processing...\n";

	//Decrypt our encrypted message
	Security::init();
	$message = Security::decrypt($message);

	//Turn the data into an array
	$data = json_decode($message, true);

	//Default response
	$response = array('status' => 'error', 'message' => 'Nothing found.');

	//Execute A Request If Exist
	if (isset($data['request'])) {

		//Request A Token
		if ($data['request'] == 'authenticate') {
			global $logins;
			global $tokens;
			if ($id = authenticate($data['user'], $data['password'], $logins)) {
				$result = createAccessToken($tokens, strtotime('+1 hour'));
				$response = $result;

			} else {
				$response = array('status' => 'error', 'message' => 'Invalid Login');
			}
		}
		else if ($data['request'] == 'refresh') {
			global $tokens;

			if (validateToken($data['refresh_token'], '*', $tokens, time())) {
				consumeToken($data['access_token'], $tokens);
				$result = createAccessToken($tokens, strtotime('+1 hour'), false);
				$response = $result;
			} else {
				$response = array('status' => 'error', 'message' => 'Invalid Token On Authorization');
			}
		}
		//Authorize a token based action
		else if ($data['request'] == 'authorize') {
			global $tokens;

			if (validateToken($data['access_token'], $data['action'], $tokens, time())) {
				consumeToken($data['access_token'], $tokens);
				$response = array('status' => 'success', 'message' => 'Access Granted');
			} else {
				$response = array('status' => 'error', 'message' => 'Invalid Token On Authorization');
			}
		}
	}

	//JSON encode the response
	echo (json_encode($response));
	$response = json_encode($response);

	//Return an encrypted message
	return Security::encrypt($response);
}, 'closure');
