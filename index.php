<?php
include('vendor/autoload.php');

use prodigyview\network\Router;
use prodigyview\network\Request;
use prodigyview\network\Response;
use prodigyview\network\Socket;
use prodigyview\system\Security;

include('authbearer/AuthBearer.php');

Router::init();
Security::init();

Router::post('/auth', array('callback' => function (Request $request) {

	$response = array();

	//RETRIEVE Data From The Request
	$data = $request->getRequestData('array');
	if ($data) {
		$data['request'] = 'authenticate';
		if($data['grant_type'] != 'authorization_code' && verifyClientCredentials($data)) {
			$response = array('status' => 'Invalid request');
		} else {
			
			//Try auth
			$result = tryToAuth($data);
	
			//Create a response from the microservice
			$response = array('status' => $result);
		}
	} else {
		$response = array('status' => 'Invalid request');
	}

	//Send response to client who accessed the API
	sendResponse(json_encode($response));
}));

Router::post('/refresh', array('callback' => function (Request $request) {

	$response = array();

	//RETRIEVE Data From The Request
	$data = $request->getRequestData('array');
	if ($data) {
		$data['request'] = 'refresh';
		//Try auth
		$result = tryToAuth($data);
	
		//Create a response from the microservice
		$response = array('status' => $result);
	} else {
		$response = array('status' => 'Invalid request');
	}

	//Send response to client who accessed the API
	sendResponse(json_encode($response));
}));

function verifyClientCredentials($data) {
	$clientId = $data['client_id'];
	$secret = $data['client_secret'];
	//Verify client credentials
	if($clientId == '123abc' && $secret == '123abc') {
		return true;
	}
	return false;
}

function tryToAuth($message)
{
	//Get payload data and attached to message
	$socket = new Socket('127.0.0.1', 8600, array('connect' => true));

	$message = json_encode($message);
	//Encrypt The Message
	Security::init();
	$message = Security::encrypt($message);

	//Send Data To Email Processing
	$result = $socket->send($message);

	//Decrypt the Encrypted message
	$result = Security::decrypt($result);

	$socket->close();

	return $result;
}

Router::setRoute();

/**
 * Open a socket to the microservice
 */
function getSocket()
{
	$host = '127.0.0.1';
	$port = 8502;
	$socket = new Socket($host, $port, array('connect' => true));

	return $socket;
}

/**
 * Send a response to the api client
 */
function sendResponse(string $message, int $status = 200)
{
	echo Response::createResponse(200, $message);
}
